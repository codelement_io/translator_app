const logger = require('../../node_modules/lp_js_logger');
const config = require('./default.json').logger;

const log = logger.getLogger({
  context: 'thd-abc-widget',
  name: 'system',
  configJSON: config
});

log.stream = {
  write: function(message, encoding){
    log.info(message.trim());
  }
};
module.exports = log;