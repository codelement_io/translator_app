export class Message{

    constructor(
                public source?:string,
                public time?:string,
                public text?:string,
                public fromLang?:string,
                public toLang?:string
                ){}
                
}

