const express = require('express');
const bodyParser = require('body-parser');
const translate = require('@google-cloud/translate');
const app  = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const port = Number(process.env.PORT || 3000)
var Message = require('./app/model/Message')
var GoogleTranslator = require('./app/translators/GoogleTranslator')
var Translator = require('./app/translators/Translator');

translator = new Translator(new GoogleTranslator());

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser());

app.use(cookieParser());

app.use(express.static(path.join(__dirname, '/dist')));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

app.use('/api/healthcheck', require('express-healthcheck')());

app.get('/api/supportedLangs', (req, res) =>{

   translator.supportedLangs()
             .then( response =>{
                    res.send(response);
             })
             .catch( error =>{
                 console.log(error);
             })
    
});

app.post('/api/translate', (req, res) =>{

  translator.translate(req)
            .then(message =>{
              res.send(message);
            }).catch(e =>{
              console.log(e);
            });

});

// Start node server
app.listen( port, function() {
  console.log( 'Node server is running on port ' + port);

});