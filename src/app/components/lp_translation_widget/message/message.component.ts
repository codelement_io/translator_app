import { Component, Input, ViewChild, ViewContainerRef, ComponentFactoryResolver, OnInit, AfterViewInit } from '@angular/core';
import { Message } from "../models/message";

@Component({
    selector: 'app-message',
    templateUrl: 'message.component.html',
    styleUrls: ['message.component.css']
})
export class MessageComponent implements OnInit{
    
    @Input()
    content: Message;

    constructor(){}

    ngOnInit(): void {
        
    }

}