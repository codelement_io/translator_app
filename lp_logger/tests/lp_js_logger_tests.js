/**
 * Created with IntelliJ IDEA.
 * User: omerh
 * Date: 1/30/14
 * Time: 2:25 PM
 * To change this template use File | Settings | File Templates.
 */
var expect = require("chai").expect;
var logger = require("../lp_js_logger");
var log = logger.getLogger({
    "name": "system",
    "context": "unit-tests",
    "configFile": "./config.json"
});

describe("FILE config tests", function () {

    describe("lp_js_loggers Logger", function () {
        it("Logger has getLogger property", function () {
            expect(logger.getLogger).to.be.a("function");
        });

        it("Logger is an object", function () {
            expect(log).to.be.a("object");
        });

        it("Logger has getLogger property", function () {
            expect(log.debug).to.be.a("function");
        });

        it("Logger has getLogger property", function () {
            expect(log.info).to.be.a("function");
        });

        it("Logger has getLogger property", function () {
            expect(log.warn).to.be.a("function");
        });

        it("Logger has getLogger property", function () {
            expect(log.error).to.be.a("function");
        });

        it("Logger has getLogger property", function () {
            expect(log.error).to.be.a("function");
        });
    });

    describe("Test logger API log level functionality", function () {
        var logOutput = {
            info: [],
            debug: [],
            warn: [],
            error: []
        };
        var msg = "Message";
        it("Test Logger debug property", function () {
            log.debug(msg);
            logOutput.debug.push(msg);
            expect(logOutput.debug).to.have.length(1);
        });
        it("Test Logger info property", function () {
            log.info(msg);
            logOutput.info.push(msg);
            expect(logOutput.info).to.have.length(1);
        });
        it("Test Logger warn property", function () {
            log.warn(msg);
            logOutput.warn.push(msg);
            log.warn(msg, { more: "info" });
            logOutput.warn.push(msg);
            expect(logOutput.warn).to.have.length(2);
        });
        it("Test Logger debug property", function () {
            log.error(msg);
            logOutput.error.push(msg);
            log.error(msg);
            logOutput.error.push(msg);
            log.error(msg, { much: "more info" });
            logOutput.error.push(msg);
            expect(logOutput.error).to.have.length(3);
        });
    });

    describe("lp_js_logger isLevel enabled", function () {

        it("Logger isDebugEnabled", function () {
            expect(log.isDebugEnabled()).to.be.false;
        });

        it("Logger isInfoEnabled", function () {
            expect(log.isInfoEnabled()).to.be.true;
        });

    });
});

