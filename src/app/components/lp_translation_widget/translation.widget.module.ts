
import {NgModule} from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ChatBoxComponent } from "./chat_box/chatbox.component";
import { InputBoxComponent } from "./input_box/inputbox.component";
import { MessageComponent } from "./message/message.component";
import { TranslatorService } from "./services/translator.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgxAutoScrollModule } from "ngx-auto-scroll";
import { CookieService } from 'ngx-cookie-service';

@NgModule({
    declarations:[
        ChatBoxComponent,
        InputBoxComponent,
        MessageComponent
    ],
    imports:[
        BrowserModule,
        FormsModule,
        NgxAutoScrollModule
    ],
    exports:[
        ChatBoxComponent,
        InputBoxComponent,
        MessageComponent
    ],
    providers:[
        TranslatorService,
        CookieService
    ],
    entryComponents:[
        MessageComponent
    ]
    
})
export class TranslationWidgetModule{

}