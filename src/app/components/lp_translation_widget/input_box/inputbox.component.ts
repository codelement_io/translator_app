
import {Component, OnInit, Output, EventEmitter} from "@angular/core";
import { TranslatorService } from "../services/translator.service";
import { Message } from '../models/message';

@Component({
    selector: 'app-inputbox',
    templateUrl: 'inputbox.component.html',
    styleUrls: ['inputbox.component.css']
})
export class InputBoxComponent implements OnInit{
    
    @Output() 
    translate = new EventEmitter();
    
    message: Message;

    constructor(private translateMessage: TranslatorService){}

    /**
     * 
     */
    ngOnInit(): void {
        
        this.message = new Message("agent",this.messageDate(),"","en","en");

    }

    /**
     * 
     * @param event 
     */
    sendMessage(event): void{

        if(this.message.text.trim().length > 0){

            this.translate.emit(this.message);
            this.message = new Message("agent",this.messageDate(),"","en","en");
        }
        
    }

    /**
     * 
     */
    messageDate(): string{

        let date = new Date();

        return date.toISOString();
    }
}