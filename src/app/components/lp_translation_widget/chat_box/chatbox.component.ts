import {Component, ViewChild, AfterViewInit, AfterContentInit, OnInit, OnDestroy, ViewContainerRef, ComponentFactoryResolver, ElementRef, ViewChildren, QueryList, ContentChildren} from "@angular/core";
import { TranslatorService } from "../services/translator.service";
import { Subscription } from "rxjs/Subscription";
import { Message } from "../models/message";
import { MessageComponent } from "../message/message.component";
import { CookieService } from "ngx-cookie-service";
import { Language } from '../models/language';

declare var lpTag: any;

/**
 * 
 */
@Component({
    selector: 'app-chatbox',
    templateUrl: 'chatbox.component.html',
    styleUrls: ['chatbox.component.css']
})
export class ChatBoxComponent implements AfterContentInit, OnDestroy{

    @ViewChild('messageContainer', {read: ViewContainerRef })
    private messageContainer;

    @ViewChild('chatbox')
    private chatbox: ElementRef;

    @ContentChildren(MessageComponent) appMessages: QueryList<MessageComponent>;

    private messagesSource: any[] = [];
    private sub: Subscription[] = [];
    private message: Message;
    private agentSdk: any = lpTag.agentSDK;
    private messageComponent: any;
    private tempSource: any = "visitor";
    private pathData: string = "chatTranscript.lines";
    private cmdName: any = lpTag.agentSDK.cmdNames.write;
    
    supportedLangs: Language[] = [];
    selectedLang: string;

    /**
     * 
     * @param translationService 
     * @param resolver 
     */
    constructor( private translationService: TranslatorService,
                 private cookieService: CookieService ,
                 private resolver: ComponentFactoryResolver ){};

    /**
     * 
     */
    ngAfterContentInit(): void {
 
        this.sub.push(
            this.translationService.getLanguages().subscribe( res =>{
                this.supportedLangs = res;

                this.setDefaultLang();
            }
        ));

        this.messageComponent = this.resolver.resolveComponentFactory(MessageComponent);

        this.agentSdk.init();
        this.agentSdk.bind(this.pathData, (data) => {
       
            if(data.newValue[0].source !== 'agent') this.pushMessage(data.newValue[0]);

        }, (error) =>{
    
        });
    }

    /**
     * 
     * @param message 
     */
    pushMessage(message: Message){

        if(message.source !== "system"){
        
            const messageCompRef = this.messageContainer.createComponent(this.messageComponent).instance;

            message = this.prepareMessage(message);

            this.translationService.translateMessage(message).subscribe(res => {

                this.appendMessage(messageCompRef, message, res);

            }, (error) => {
    
                console.log(error);
            })

        }
        
    }

    /**
     * 
     * @param message 
     */
    prepareMessage(message): Message{

        message.text = this.tripHtml(message.text);
        message.time = message.time.split('T')[1].split('.')[0];

        return message;
    }
    
    /**
     * 
     * @param messageCompRef 
     * @param message 
     * @param res 
     */
    appendMessage(messageCompRef, message, res){

        switch(message.source){ 

            case "agent":
                    message.toLang = res.toLang;
                    messageCompRef.content = message;
                    this.agentSdk.command(this.cmdName, {text: res.text});            
                break;

            case "visitor":
                    messageCompRef.content = res;
                break;

        }
        
        this.trackMsgSource(messageCompRef);
    }

    /**
     * 
     * @param component 
     */
    trackMsgSource(component){

        this.messagesSource.push(component.content.source);

        if(this.messagesSource.length == 2){

            if(this.messagesSource[0] == this.messagesSource[1]){
                component.sameSource = true;    
            }else{
                component.sameSource = false;
            }

            this.messagesSource = [];
        }
    }

    /**
     * 
     */
    onLangSelected(language): void{
        this.selectedLang = language;
        this.cookieService.set("agent_lang", this.selectedLang);
    }

    /**
     * 
     */
    setDefaultLang(): void{

        if(this.cookieService.get("agent_lang")){
            this.supportedLangs.forEach( lang =>{
                if(lang.code == this.cookieService.get("agent_lang")){
                    this.selectedLang = lang.name;
                }
            });
            
        }else{
            this.selectedLang = "English";
            this.cookieService.set("agent_lang","en");
        }
    }

    /**
     * 
     * @param msg 
     */
    tripHtml(msg: string): string{

        return msg.replace(/<{1}[^<>]{1,}>{1}/g," ");
    } 

    /**
     * 
     */
    ngOnDestroy(): void {

        this.sub.forEach(subscription => subscription.unsubscribe());

        this.agentSdk.init();
        this.agentSdk.unbind(this.pathData);
    }
}