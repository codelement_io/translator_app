# LP Logger for NodeJS projects
![alt text][1]

 [1]: http://ctvr-ci:3000/?type=npm&artifact=lp_js_logger ("NPM")

Module containing the functionality for logging when creating a nodeJS projects, the module wraps the Winston module logging(a multi-transport async logging library for node.js)

## Getting Started
Add in your package.json depency to the LP Logger module with the correct version
* "dependencies": {
        "lp_js_logger" : "latest"
    }
* change the version with the desired version to work with

### Extra functions
- isDebugEnabled()
- isInfoEnabled()

### Example configuration
```json
{
  "logging": {
    "system": {
      "console": {
        "level": "debug",
        "colorize": true
      },
      "hourlyRotateFile": {
        "level": "debug",
        "timestamp": true,
        "json": true,
        //allows tailing on the file name, where after rotating the current one will still be called filename
        "tailable": true,
        "filename": "logs/pusher",
        //defines the rotation if you provide HH will be rotated every hour, mm - every minute and so on...
        "datePattern": ".yyyy-MM-dd-HH.log"
      }
    }
  }
}
```

# Dependencies

* [winston](http://github.com/flatiron/winston) - a multi-transport async logging library for node.js
* [winston-logstash](https://github.com/jaakkos/winston-logstash) - a logstash transport for winston (local copy with bug fixes)
* [winston-config](http://github.com/triplem/winston-config) - a config loader for logging library for node.js
