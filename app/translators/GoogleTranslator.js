const TranslatorUtil = require('../util/TranslatorWidgetUtil');
const languages = require('google-translate-api/languages');
const Translate = require('@google-cloud/translate');
const logger = require('../util/logger').log;
const path = require("path")
const projectId = 'lp-translate-api';
const keyFilename = path.join(__dirname, '/resources/lp-translate-api-4b7700500d4a.json');

const translate = new Translate({
  projectId: projectId,
  keyFilename: keyFilename
});

var visitorLang = "en";
var agentLang;

var GoogleTranslator = function() {

    this.translate = (req) => {

        message = req.body;
        agentLang = req.cookies['agent_lang'];
        
        switch(message.source){

            case "visitor":

                return translate.translate(message.text, agentLang)
                                .then(res => {
                                    return getMessage(message, res);
                                }).catch(err => {
                                    console.error(err);
                                });
                break;

            case "agent":

                return translate.translate(message.text, visitorLang)
                                .then(res => {
                                    return getMessage(message, res);
                                }).catch(err => {
                                    console.error(err);
                                });
                 break;

            default:
                    break;

        }

    }

    this.supportedLangs = () => {

       return translate.getLanguages()
                       .then(results => {
                            return results[0];
                        })
                        .catch(err => {
                            console.error('ERROR:', err);
                        });
    }

    var getMessage = (message, res) => {

        translation = getTranslation(res);
        message.text = translation.translatedText;
        message.fromLang = translation.detectedSourceLanguage;

        setToLang(message);
        
        setVisitorLang(message);

        return message;
    }

    var setToLang = (message) =>{

        switch(message.source){

            case "visitor":    
                message.toLang = agentLang;
                break;

            case "agent":
                message.toLang = visitorLang;
                break;
        }
       
    }
    var getTranslation = (res) =>{

        if(res !== undefined){

            return res[1].data.translations[0];
        }else{
            return "";
        }
        
    }
    var setVisitorLang = (message) => { 
        message.source == "visitor" ? visitorLang = message.fromLang : message.fromLang = message.fromLang 
    }

}


module.exports = GoogleTranslator;