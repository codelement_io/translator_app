"use strict"

class Message{

    constructor(text, fromLang, toLang){
        
        this.text = text;
        this.fromLang = fromLang;
        this.toLang = toLang;
    }
}

module.exports = Message;