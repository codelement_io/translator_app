import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { Http  } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Message } from "../models/message";
import 'rxjs/Rx';
import { flatMap } from 'rxjs/operators';
import { Language } from "../models/language";

@Injectable()
export class TranslatorService{

    private messageSubject: Subject<any>;

    constructor(private _http: Http){
        this.messageSubject = new Subject<any>();
    }

    public translateMessage(message: Message): Observable<Message>{
        return this._http.post("/api/translate",message).map(res => {
            const{ source,time,text,fromLang,toLang } = res.json();
            return new Message(source,time,text,fromLang,toLang)

        });
    }

    public getLanguages(): Observable<Language[]>{
        return this._http.get("/api/supportedLangs").map(res => res.json());
    }
}