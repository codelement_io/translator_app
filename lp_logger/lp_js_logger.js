/**
 * Created with IntelliJ IDEA.
 * User: omerh, itaik
 * Date: 07/01/14
 * Time: 10:05 AM
 * To change this template use File | Settings | File Templates.
 */
var path = require('path');
var os = require('os');
var fs = require('fs');
var util = require('util');

var common = require('winston/lib/winston/common');
var config = require('./config');

var winston = require('winston');

//
// Requiring 'hourly-rotate-file' will expose
// `winston.transports.HourlyRotateFile`    
//
require('./transports/hourly-rotate-file');

require('./transports/winston-logstash');

// internal variables - used only by lp_js_logger.js
var winstonLogger;
var hostname;

var supportedLevels = exports.supportedLevels = config.levels;

// Keep the original log method for building the default winston log structure
var defLog = common.log;

function _getJSON(str) {
    var json;

    try {
        json = JSON.parse(str);
    }
    catch (e) {
    }

    return json;
}

function _resolveHostName() {
    try {
        return '[' + os.hostname() + ']';
    }
    catch (e) {
        return '[error Getting the hostname of the machine]';
    }
}

function _getValidLevel(level) {
    if (level && 'string' === typeof level) {
        return level.toLowerCase();
    }

    return "info";
}

function _logMsg(logger, context, msg, lvl) {
    var level = _getValidLevel(lvl);
    var objMsg = _getJSON(msg);
    if (objMsg) {
        objMsg.application_context = hostname + context;
        msg = JSON.stringify(objMsg);
    }
    else {
        msg = hostname + context + ':' + msg;
    }

    logger[level](msg);
}

function _isLevelEnabled(logger, lvl) {
    var transport;
    var transportLevel;
    var loggerLevel;
    var result = false;
    var level = _getValidLevel(lvl);

    for (var transportName in logger.transports) {
        if (logger.transports.hasOwnProperty(transportName)) {
            transport = logger.transports[transportName];
            transportLevel = _getValidLevel(transport.level);
            loggerLevel = _getValidLevel(logger.level);

            if ((transportLevel && logger.levels[transportLevel] >= logger.levels[level]) ||
                (!transportLevel && logger.levels[loggerLevel] >= logger.levels[level])) {
                result = true;
                break;
            }
        }
    }

    return result;
}

function _transportErrorHandler(transport, transportName) {
    transport.on('error', function (err) {
        console.log('Transport Error occurred:' + err + ' name:' + transportName);
    });
}

function _configureWinston(winstonConf) {
    var transports;

    winstonConf = winstonConf.logging;

    for (var loggerName in winstonConf) {
        if (winstonConf.hasOwnProperty(loggerName)) {
            transports = winstonConf[loggerName];

            for (var transport in transports) {
                if (transports.hasOwnProperty(transport)) {
                    if ('string' === typeof transports[transport].level) {
                        transports[transport].level = _getValidLevel(transports[transport].level);
                    }
                }
            }

            winston.loggers.add(loggerName, transports);
        }
    }

    return winston;
}

function _getDate(date) {
    var dt = Date.parse(date);

    return !isNaN(dt) && new Date(dt);
}

function _dateFormat(date, format) {
    var dt = _getDate(date);

    if (dt && dt instanceof Date) {
        return format.replace(/%[yMdHmsS]/g, function (elem) {
            var datePart;
            var slice = -2;

            switch (elem) {
                case '%y':
                    return dt.getFullYear(); // no leading zeros required
                case '%M':
                    datePart = 1 + dt.getMonth();
                    break;
                case '%d':
                    datePart = dt.getDate();
                    break;
                case '%H':
                    datePart = dt.getHours();
                    break;
                case '%m':
                    datePart = dt.getMinutes();
                    break;
                case '%s':
                    datePart = dt.getSeconds();
                    break;
                case '%S':
                    slice = -3;
                    datePart = dt.getMilliseconds();
                    break;
                default:
                    slice = 1;
                    return elem.slice(slice); // unknown code, remove %
            }

            // add leading zero if required
            return ('0' + datePart).slice(slice);
        });
    }
    else {
        return date;
    }
}

function _getTimestamp(dateFormat) {
    return function() {
        return _dateFormat(new Date().toISOString(), dateFormat || '%d-%M-%y %H:%m:%s.%S');
    };
}

// Override log method and deflate the json structure so logstash will not be upset
common.log = function (options) {

    options.timestamp = (!!options.timestamp || 'undefined' === typeof options.timestamp || null === options.timestamp) ? _getTimestamp(options.dateFormat) : options.timestamp;

    // First, call the default log method to build the default initial structure for logging
    var defaultLog = defLog(options);

    // We are only interested in JSON logs
    if (options.json) {
        var line = _getJSON(defaultLog) || {};
        var message = line.message || '';

        // Fix level if any
        if (line.level) {
            line.level = _getValidLevel(line.level);
        }

        // Check that it is a json
        if (/^[\],:{}\s]*$/.test(message.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

            // Message is JSON
            // Parse the internal JSON message and add the matadata
            message = Object.assign({}, options.meta || {}, _getJSON(message) || {});

            // Set the message attribute and remove msg for consistency
            message.message = message.msg;
            delete message.msg;

            // Add the level and timestamp from winston structure
            message.level = line.level;
            message.timestamp = line.timestamp;

            // return the flattened structure
            return JSON.stringify(message);
        }

        return JSON.stringify(line);
    }

    // Return the default structure
    return defaultLog;
};

exports.getLogger = function (options) {
    var selectedLog;
    var logger;
    var configFile;
    var levels;
    var transportName;

    // Set Context
    var context = options.context ? ':[' + options.context + ']' : '';

    if ('string' === typeof options) {
        selectedLog = options;
    }
    else {
        options = options || {};
        selectedLog = options.name;
    }

    if (options.configFile) {
        configFile = path.resolve(options.configFile);
        var winstonConf;
        try {
            winstonConf = JSON.parse(fs.readFileSync(configFile, 'utf-8'));
        }
        catch (ex) {
            console.error('error during winston configuration', ex);
        }

        winstonLogger = _configureWinston(winstonConf);
    }
    else if (options.configJSON) {
        winstonLogger = _configureWinston(options.configJSON);
    }

    logger = winstonLogger.loggers.get(selectedLog);

    if (!options.transports && !logger && config.logging) {
        logger = winstonLogger.loggers.add(selectedLog, config.logging).get(selectedLog);
    }
    else if (options.transports) {
        // only allow overriding all the config
        for (transportName in options.transports) {
            logger.transports[transportName] = options.transports[transportName];
        }
    }

    // Resolve the host name
    hostname = hostname || _resolveHostName();

    for (transportName in logger.transports) {
        if (logger.transports[transportName].on) {
            _transportErrorHandler(logger.transports[transportName], transportName);
        }
    }

    // Build the logger levels object returned to the client
    levels = {};

    // we want to support only debug/info/warn/error levels (defined in the config file)
    for (var lvl in supportedLevels) {
        if (lvl && supportedLevels.hasOwnProperty(lvl)) {
            /*jshint -W083 */
            (function (lev) {
                var level = _getValidLevel(lev);
                levels[level] = function (msg, meta) {
                    var message = msg + (meta && (' ' + util.inspect(meta)) || '');
                    _logMsg(logger, context, message, level);
                };
            })(supportedLevels[lvl]);
            /*jshint +W083 */
        }
    }

    levels.isDebugEnabled = function () {
        return _isLevelEnabled(logger, 'debug');
    };

    levels.isInfoEnabled = function () {
        return _isLevelEnabled(logger, 'info');
    };

    return levels;
};
