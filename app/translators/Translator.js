var Translator = function(adapter){
    this.adapter = adapter;
}

Translator.prototype.translate = function(req){

    return this.adapter.translate(req);
}

Translator.prototype.supportedLangs = function(){

    return this.adapter.supportedLangs();
}

module.exports  = Translator